Meta:
@author dieter spaeth

Erzählung:
Als ein service-user
Möchte ich ...
So dass ...

Szenario: Wenn Ein Nutzer, ein nicht existierendes repository von github abruft, so wird github mit 'not found' antworten.

Gegeben ist ein github query service
Und ein zufälliger, nicht existierender username/repository
Wenn ich das zufällige username/repository, mit Hilfe des Service abrufe,
Dann antwortet der Service mit: {
    'owner': '*',
    'repo': '*',
	'stars': '',
	'watchers': '',
	'forks': '',
	'last': ''
}

Wenn ich das xxx/xxx, mit Hilfe des Service abrufe,
Dann antwortet der Service mit: {
    'owner': 'xxx',
    'repo': 'xxx',
	'stars': '',
	'watchers': '',
	'forks': '',
	'last': ''
}

Wenn ich das xxx2/xxx, mit Hilfe des Service abrufe,
Dann antwortet der Service mit: {
    'owner': 'xxx2',
    'repo': 'xxx',
	'stars': '',
	'watchers': '',
	'forks': '',
	'last': ''
}
