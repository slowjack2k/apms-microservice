Meta:
@author dieter spaeth

Narrative:
As a service-user
I want to look up a non-existent repository with the github query service
So that I can be sure that the repository can not be found on github

Scenario: when a user checks a non-existent repository on github, github would respond 'not found'

Given github query service
And a random non-existent username/repository
When I look for the random username/repository via the service
Then the service respond: {
    'owner': '*',
    'repo': '*',
	'stars': '',
	'watchers': '',
	'forks': '',
	'last': ''
}

When I look for xxx/xxx via the service
Then the service respond: {
    'owner': 'xxx',
    'repo': 'xxx',
	'stars': '',
	'watchers': '',
	'forks': '',
	'last': ''
}

When I look for xxx2/xxx via the service
Then the service respond: {
    'owner': 'xxx2',
    'repo': 'xxx',
	'stars': '',
	'watchers': '',
	'forks': '',
	'last': ''
}
