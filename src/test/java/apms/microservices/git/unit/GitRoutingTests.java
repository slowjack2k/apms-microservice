package apms.microservices.git.unit;

import static apms.transport.utils.JSONUtils.asJSON;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import apms.microservices.git.config.GitMicroserviceConfig;
import apms.microservices.git.service.GitRouter;
import apms.transport.TransportComponentScanEntryPoint;

@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationComponentScan
@ComponentScan(basePackageClasses = { GitMicroserviceConfig.class, TransportComponentScanEntryPoint.class })
@SuppressWarnings("serial")
public class GitRoutingTests {

	@Autowired
	private GitRouter matcher;

	@Test
	public void it_handles_git_get_msgs() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "git");
				put("cmd", "get");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("gitCmdGetInputChannel");
	}

	@Test
	public void it_handles_git_query_msgs() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "git");
				put("cmd", "query");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("gitCmdQueryInputChannel");
	}

	@Test
	public void it_handles_git_query_msgs_with_url() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "git");
				put("cmd", "get");
				put("giturl", "git://");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("gitCmdGetWithUrlInputChannel");
	}

	@Test
	public void it_handles_info_need_part_query_msgs() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "info");
				put("need", "part");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("gitInfoNeedPartQueryInputChannel");
	}

	@Test
	public void it_handles_extract_query_msgs() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "git");
				put("cmd", "extract");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("gitCmdExtractInputChannel");
	}

	@Test
	public void it_rejects_other_msgs() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "npm");
				put("cmd", "info");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("withoutDestination");
	}
}
