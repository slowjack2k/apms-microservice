package apms.microservices.git.unit;

import static apms.microservices.git.service.GitCmdGetListener.GIT_URL_PATTERN;
import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.regex.Matcher;

import org.junit.Test;

import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;

public class GitUrlPatternTests {

	@Test
	public void it_matches_github_urls() {
		Matcher m = GIT_URL_PATTERN.matcher("git+https://github.com/theOwner/theRepo.git");

		assertThat(m.find()).isEqualTo(true);
		assertThat(m.group(1)).isEqualTo("theOwner");
		assertThat(m.group(2)).isEqualTo("theRepo");
		assertThat(m.group(3)).isEqualTo(".git");
	}

	public static class MethodNameProxy {
		public static <I> I wrap(Class<?> clazz, I[] placeholder) {
			return wrapInternally(clazz, placeholder);
		}

		@SuppressWarnings("unchecked")
		private static <I> I wrapInternally(Class<?> clazz, I[] placeholder) {
			ProxyFactory factory = new ProxyFactory();
			factory.setSuperclass(clazz);

			MethodHandler handler = new MethodHandler() {
				@Override
				public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
					return thisMethod.getName();
				}
			};

			try {
				return (I) factory.create(new Class<?>[0], new Object[0], handler);
			} catch (NoSuchMethodException | IllegalArgumentException | InstantiationException | IllegalAccessException
					| InvocationTargetException e) {

				e.printStackTrace();
			}

			return null;
		}
	}

	@Test
	public void proxy_test() {
		assertThat(MethodNameProxy.wrap(GitUrlPatternTests.class, new GitUrlPatternTests[0]).blubberNew())
				.isEqualTo("blubber");
	}

	protected String blubberNew() {
		return "blubber blasster";
	}
}
