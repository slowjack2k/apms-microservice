package apms.microservices.git.steps;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jbehave.core.annotations.AsJson;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import apms.microservices.git.service.GitCmdQueryListener;

@Steps
public class GithubRepoNotFoundStepsDe {
	private Map<String, String> cmd;
	private Message<String> response;
	private final static ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private GitCmdQueryListener gitCmdQueryListener;

	@Given("ist ein github query service")
	public void givenGithubUserProfileApi() {
		this.cmd = new HashMap<>();

	}

	@Given("ein zufälliger, nicht existierender username/repository")
	public void givenANonexistentRepository() {
		cmd.put("owner", randomAlphabetic(8));
		cmd.put("repo", randomAlphabetic(8));
	}

	@When("ich das zufällige username/repository, mit Hilfe des Service abrufe,")
	public void whenILookForTheUserViaTheApi() throws IOException {
		response = gitCmdQueryListener.handleCmdQuery(cmd, new HashMap<String, Object>());
	}

	@When("ich das $username/$repository, mit Hilfe des Service abrufe,")
	public void whenILookForSomeNonExistentUserViaTheApi(String username, String repository) throws IOException {
		cmd.put("owner", username);
		cmd.put("repo", repository);
		response = gitCmdQueryListener.handleCmdQuery(cmd, new HashMap<String, Object>());
	}

	@Then("antwortet der Service mit: $json")
	public void thenGithubRespond404NotFound(JsonResponse json)
			throws JsonParseException, JsonMappingException, IOException {
		assertThat(responseAsJson(response)).as("Response").isEqualTo(json);
	}

	private JsonResponse responseAsJson(Message<String> response2)
			throws JsonParseException, JsonMappingException, IOException {
		JsonResponse result = null;

		result = mapper.readValue(response.getPayload(), JsonResponse.class);

		return result;
	}

	@AsJson
	public static class JsonResponse {
		private String owner;
		private String repo;
		private String stars;
		private String watchers;
		private String forks;
		private String last;

		public String getOwner() {
			return owner;
		}

		public String getRepo() {
			return repo;
		}

		public String getStars() {
			return stars;
		}

		public String getForks() {
			return forks;
		}

		public String getLast() {
			return last;
		}

		public String getWatchers() {
			return watchers;
		}

		@Override
		public String toString() {
			return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
		}

		@Override
		public int hashCode() {
			Collection<String> exclude = new ArrayList<>();
			if (isWildCard(this.owner)) {
				exclude.add("owner");
			}
			if (isWildCard(this.repo)) {
				exclude.add("repo");
			}
			return HashCodeBuilder.reflectionHashCode(this, exclude);
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (obj == this) {
				return true;
			}
			if (obj.getClass() != getClass()) {
				return false;
			}
			JsonResponse rhs = (JsonResponse) obj;

			Collection<String> exclude = new ArrayList<>();
			if (isWildCard(this.owner) || isWildCard(rhs.owner)) {
				exclude.add("owner");
			}
			if (isWildCard(this.repo) || isWildCard(rhs.owner)) {
				exclude.add("repo");
			}

			return EqualsBuilder.reflectionEquals(this, obj, exclude);
		}

		private boolean isWildCard(String look) {
			return look != null && look.equals("*");
		}

	}

}
