package apms.microservices.git.acceptance;

import java.util.Locale;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import apms.microservices.git.config.GitMicroserviceConfig;
import apms.microservices.git.stories.AbstractLocalizedSpringJBehaveStory;
import apms.microservices.info.config.matchers.InfoCmdCollectMatcher;
import apms.microservices.npm.config.matchers.NpmCmdGetMatcher;
import apms.transport.TransportComponentScanEntryPoint;
import apms.utils.AcceptanceTest;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { GitMicroserviceConfig.class, NpmCmdGetMatcher.class, InfoCmdCollectMatcher.class,
		TransportComponentScanEntryPoint.class })
@AcceptanceTest
public class GithubRepoNotFoundDe extends AbstractLocalizedSpringJBehaveStory {
	@Override
	protected Locale locale() {
		return new Locale("de");
	}
}
