package apms.microservices.git.acceptance;

import static apms.transport.utils.JSONUtils.asJSON;
import static apms.transport.utils.MessageHelper.buildMessage;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.http.MediaType;
import org.springframework.integration.IntegrationMessageHeaderAccessor;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import apms.microservices.git.config.GitMicroserviceConfig;
import apms.microservices.git.domain.NpmModuleGitRepository;
import apms.microservices.info.config.matchers.InfoCmdCollectMatcher;
import apms.microservices.npm.config.matchers.NpmCmdGetMatcher;
import apms.transport.TransportComponentScanEntryPoint;

@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationComponentScan
@SpringBootTest(classes = { GitMicroserviceConfig.class, NpmCmdGetMatcher.class, InfoCmdCollectMatcher.class,
		TransportComponentScanEntryPoint.class })
@SuppressWarnings("serial")
public class GitCmdNeedPartTests {
	private static CountDownLatch latch;

	@Configuration
	static class TestConfig {
		@Bean
		public MessageChannel npmCmdGetInputChannel() {
			return new DirectChannel();
		}

		@Bean
		public MessageChannel infoCmdCollectInputChannel() {
			return new ExecutorChannel(new SimpleAsyncTaskExecutor());
		}

	}

	@MessageEndpoint
	static class TestNpmCmdGetInputActivator {
		@ServiceActivator(inputChannel = "npmCmdGetInputChannel", outputChannel = "msgToJsonConverterChannel")
		public Message<Map<String, String>> handleCmdQuery(@Payload Object cmd, @Headers Map<String, Object> headers) {
			Map<String, String> result = new HashMap<>();
			result.put("giturl", "git+https://github.com/expressjs/express.git");

			return buildMessage(result, headers);
		}
	}

	@MessageEndpoint
	static class TestNpmInfoCmdCollect {
		private String result;

		public void resetResult() {
			result = null;
		}

		public String getResult() {
			return result;
		}

		@ServiceActivator(inputChannel = "infoCmdCollectInputChannel")
		public void handleCmdQuery(@Payload String result, @Headers Map<String, Object> headers) {
			this.result = result;
			latch.countDown();
		}
	}

	@MessagingGateway(defaultRequestChannel = "gitInfoNeedPartQueryInputChannel")
	public interface TestInfoNeeded {
		@Gateway(replyTimeout = 10000, replyChannel = "jsonToMapConverterChannel")
		public void sendInfoNeeded(Message<String> msg);
	}

	@Autowired(required = true)
	private TestInfoNeeded sender;

	@Autowired(required = true)
	private NpmModuleGitRepository repo;

	@Autowired(required = true)
	@Qualifier("infoCmdCollectInputChannel")
	private MessageChannel infoCmdCollectInputChannel;

	@Autowired
	private TestNpmInfoCmdCollect collector;

	private MockRestServiceServer server;

	@Autowired
	private RestTemplate restTemplate;

	@Before
	public void setUp() {
		latch = new CountDownLatch(1);
		collector.resetResult();
		server = MockRestServiceServer.createServer(restTemplate);
		this.server.expect(requestTo("https://api.github.com/repos/expressjs/express"))
				.andRespond(withSuccess(
						"{\n" + "  \"pushed_at\": \"2015-09-15T19:42:55Z\",\n" + "  \"forks_count\": 5,\n"
								+ "  \"stargazers_count\": 10,\n" + "  \"subscribers_count\": 1\n" + "}",
						MediaType.APPLICATION_JSON));
	}

	@Test
	public void it_converts_a_json_msg_to_an_object_msg() throws InterruptedException {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "info");
				put("need", "part");
				put("name", "express");
			}
		};

		Map<String, String> expectedResultFromRepo = new HashMap<String, String>() {
			{
				put("forks", "5");
				put("last", "2015-09-15T19:42:55Z");
				put("owner", "expressjs");
				put("repo", "express");
				put("stars", "10");
				put("watchers", "1");
			}
		};

		MessageBuilder<String> infoMsg = MessageBuilder.withPayload(asJSON(msg));
		infoMsg.setHeader(IntegrationMessageHeaderAccessor.CORRELATION_ID, "0000-0000-0000");
		infoMsg.setHeader("xProducer", "0000-0000-0000");

		sender.sendInfoNeeded(infoMsg.build());

		latch.await(5, TimeUnit.SECONDS);

		server.verify();

		assertThat(repo.findByName("express").get().getDataAsMap()).isEqualTo(expectedResultFromRepo);
		assertThat(collector.getResult()).isEqualTo(
				"{\"role\":\"info\",\"data\":\"{\\\"owner\\\":\\\"expressjs\\\",\\\"forks\\\":\\\"5\\\",\\\"last\\\":\\\"2015-09-15T19:42:55Z\\\",\\\"repo\\\":\\\"express\\\",\\\"watchers\\\":\\\"1\\\",\\\"stars\\\":\\\"10\\\"}\",\"part\":\"git\",\"name\":\"express\",\"collect\":\"part\"}");

	}
}
