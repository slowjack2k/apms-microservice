package apms.microservices.git.stories;

import static org.jbehave.core.reporters.Format.HTML;
import static org.jbehave.core.reporters.Format.IDE_CONSOLE;
import static org.jbehave.core.reporters.Format.TXT;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.Keywords;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.EmbedderControls;
import org.jbehave.core.i18n.LocalizedKeywords;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.ResourceLoader;
import org.jbehave.core.io.StoryLoader;
import org.jbehave.core.io.StoryPathResolver;
import org.jbehave.core.io.UnderscoredCamelCaseResolver;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.model.ExamplesTableFactory;
import org.jbehave.core.model.TableTransformers;
import org.jbehave.core.parsers.RegexStoryParser;
import org.jbehave.core.reporters.FilePrintStreamFactory.ResolveToPackagedName;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.ParameterControls;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.ParameterConverters.ExamplesTableConverter;
import org.jbehave.core.steps.ParameterConverters.NumberConverter;
import org.jbehave.core.steps.ParameterConverters.ParameterConverter;
import org.jbehave.core.steps.spring.SpringStepsFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class AbstractLocalizedSpringJBehaveStory extends JUnitStory {
	private static final int STORY_TIMEOUT = 120;
	private static final int STORY_THREADS = 10;

	@Autowired
	protected ApplicationContext applicationContext;

	public AbstractLocalizedSpringJBehaveStory() {
		Embedder embedder = new Embedder();
		embedder.useEmbedderControls(embedderControls());
		embedder.useMetaFilters(Arrays.asList("-skip"));
		useEmbedder(embedder);
	}

	@Override
	public Configuration configuration() {
		return new MostUsefulConfiguration().useStoryPathResolver(storyPathResolver()).useStoryLoader(storyLoader())
				.useStoryReporterBuilder(storyReporterBuilder()).useKeywords(keywords())
				.useStoryParser(new RegexStoryParser(keywords(), resourceLoader(), tableTransformers()))
				.useTableTransformers(tableTransformers()).useParameterConverters(parameterConverters())
				.useParameterControls(parameterControls());
	}

	private ParameterControls parameterControls() {
		return new ParameterControls();
	}

	private ParameterConverters parameterConverters() {
		return new ParameterConverters(resourceLoader(), parameterControls(), tableTransformers(), true)
				.addConverters(customConverters(keywords(), resourceLoader(), tableTransformers()));
	}

	protected Keywords keywords() {
		return new LocalizedKeywords(locale());

	}

	protected Locale locale() {
		return new Locale("en");
	}

	protected LoadFromClasspath resourceLoader() {
		ClassLoader classLoader = this.getClass().getClassLoader();
		return new LoadFromClasspath(classLoader);
	}

	protected TableTransformers tableTransformers() {
		return new TableTransformers();
	}

	@SuppressWarnings("rawtypes")
	private ParameterConverter[] customConverters(Keywords keywords, ResourceLoader resourceLoader,
			TableTransformers tableTransformers) {
		return new ParameterConverter[] { new NumberConverter(NumberFormat.getInstance(locale())),
				new ExamplesTableConverter(new ExamplesTableFactory(keywords, resourceLoader, tableTransformers)) };
	}

	@Override
	public InjectableStepsFactory stepsFactory() {
		return new SpringStepsFactory(configuration(), applicationContext);
	}

	private EmbedderControls embedderControls() {
		return new EmbedderControls().doIgnoreFailureInView(false).useThreads(STORY_THREADS)
				.useStoryTimeouts(Integer.toString(STORY_TIMEOUT));
	}

	private StoryPathResolver storyPathResolver() {
		return new UnderscoredCamelCaseResolver();
	}

	private StoryLoader storyLoader() {
		return new LoadFromClasspath();
	}

	protected StoryReporterBuilder storyReporterBuilder() {
		Properties properties = new Properties();
		properties.setProperty("reports", "ftl/jbehave-reports.ftl");
		properties.setProperty("encoding", "UTF-8");

		return new StoryReporterBuilder().withCodeLocation(CodeLocations.codeLocationFromClass(this.getClass()))
				.withPathResolver(new ResolveToPackagedName()).withFailureTrace(true).withDefaultFormats()
				.withFormats(IDE_CONSOLE, TXT, HTML).withViewResources(properties).withKeywords(keywords());
	}
}
