package apms.microservices.info.integration;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import apms.microservices.info.config.InfoMicroserviceConfig;
import apms.microservices.info.config.RedisConfig;
import apms.microservices.info.domain.DataRepository;
import apms.microservices.info.domain.ModuleInfos;
import apms.transport.TransportComponentScanEntryPoint;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { InfoMicroserviceConfig.class, TransportComponentScanEntryPoint.class,
		RedisConfig.class }, webEnvironment = WebEnvironment.NONE)
@ActiveProfiles("test")
public class CachingTests {
	@Autowired
	private DataRepository repo;

	@Autowired
	private RedisTemplate<String, Object> template;

	@SuppressWarnings("serial")
	@Test
	public void an_entity_can_be_stored() throws InterruptedException {
		Map<String, String> data = new HashMap<String, String>() {
			{
				put("git", "{}");
				put("npm", "{1: 2}");
			}
		};
		ModuleInfos infos = new ModuleInfos("express", data);

		repo.save(infos);

		Set<byte[]> keys = template.getConnectionFactory().getConnection().keys("*".getBytes());

		System.out.println("-------");
		for (byte[] key : keys) {

			String strKey = new String(key, 0, key.length);
			System.out.println(strKey + " -> " + template.getExpire(strKey, TimeUnit.MINUTES));
		}
		System.out.println("-------");

		assertThat(repo.findById("express").get().getData().get("npm")).isEqualTo("{1: 2}");
	}

}
