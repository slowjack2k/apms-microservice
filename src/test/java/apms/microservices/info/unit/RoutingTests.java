package apms.microservices.info.unit;

import static apms.transport.utils.JSONUtils.asJSON;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import apms.microservices.info.config.InfoMicroserviceConfig;
import apms.microservices.info.service.InfoRouter;
import apms.transport.TransportComponentScanEntryPoint;

@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationComponentScan
@ComponentScan(basePackageClasses = { InfoMicroserviceConfig.class, TransportComponentScanEntryPoint.class })
@SuppressWarnings("serial")
public class RoutingTests {

	@Autowired
	private InfoRouter matcher;

	@Test
	public void it_handles_info_get_msgs() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "info");
				put("cmd", "get");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("infoCmdGetInputChannel");
	}

	@Test
	public void it_handles_info_collect_msgs() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "info");
				put("collect", "part");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("infoCmdCollectInputChannel");
	}
}
