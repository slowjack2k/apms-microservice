package apms.microservices.info.acceptance;

import static apms.transport.utils.JSONUtils.asJSON;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.integration.IntegrationMessageHeaderAccessor;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import apms.microservices.info.config.InfoMicroserviceConfig;
import apms.microservices.npm.config.NpmMicroserviceConfig;
import apms.microservices.npm.domain.NpmModuleRepository;
import apms.transport.TransportComponentScanEntryPoint;

@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationComponentScan
@SpringBootTest(classes = { InfoMicroserviceConfig.class, NpmMicroserviceConfig.class,
		TransportComponentScanEntryPoint.class })
@SuppressWarnings("serial")
public class QueryInfoTests {
	@Autowired(required = true)
	@Qualifier("infoCmdGetTransformedOutputChannel")
	private MessageChannel infoCmdGetTransformedOutputChannel;

	@Autowired(required = true)
	private NpmModuleRepository repo;

	@Test
	public void it_converts_a_json_msg_to_an_object_msg() throws InterruptedException {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "info");
				put("cmd", "get");
				put("name", "express");
			}
		};

		MessageBuilder<String> infoMsg = MessageBuilder.withPayload(asJSON(msg));
		infoMsg.setHeader(IntegrationMessageHeaderAccessor.CORRELATION_ID, "0000-0000-0000");
		infoMsg.setHeader("xProducer", "0000-0000-0000");

		infoCmdGetTransformedOutputChannel.send(infoMsg.build());

		infoMsg.setHeader(IntegrationMessageHeaderAccessor.CORRELATION_ID, "0000-0000-0001");

		infoCmdGetTransformedOutputChannel.send(infoMsg.build());

		Thread.sleep(8000);

		assertThat(repo.findByName("express").getDataAsMap().get("name")).isEqualTo("express");

		infoMsg.setHeader(IntegrationMessageHeaderAccessor.CORRELATION_ID, "1000-0000-0000");

		infoCmdGetTransformedOutputChannel.send(infoMsg.build());

		Thread.sleep(5000);

	}
}
