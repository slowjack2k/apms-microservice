package apms.microservices.npm.acceptance;

import static apms.transport.utils.JSONUtils.asJSON;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.integration.IntegrationMessageHeaderAccessor;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import apms.microservices.npm.config.NpmMicroserviceConfig;
import apms.microservices.npm.domain.NpmModuleRepository;
import apms.transport.TransportComponentScanEntryPoint;

@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationComponentScan
@ComponentScan(basePackageClasses = { NpmMicroserviceConfig.class, TransportComponentScanEntryPoint.class })
@SuppressWarnings("serial")
public class QueryInfoPartsTests {
	@Autowired(required = true)
	@Qualifier("infoNeedPartQueryInputChannel")
	private MessageChannel infoNeedPartQueryInputChannel;

	@Autowired(required = true)
	private NpmModuleRepository repo;

	@Test
	public void it_converts_a_json_msg_to_an_object_msg() throws InterruptedException {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "info");
				put("need", "part");
				put("name", "express");
			}
		};

		MessageBuilder<String> infoMsg = MessageBuilder.withPayload(asJSON(msg));
		infoMsg.setHeader(IntegrationMessageHeaderAccessor.CORRELATION_ID, "0000-0000-0000");
		infoMsg.setHeader("xProducer", "0000-0000-0000");

		infoNeedPartQueryInputChannel.send(infoMsg.build());

		infoMsg.setHeader(IntegrationMessageHeaderAccessor.CORRELATION_ID, "0000-0000-0001");

		infoNeedPartQueryInputChannel.send(infoMsg.build());

		Thread.sleep(8000);

		assertThat(repo.findByName("express").getDataAsMap().get("name")).isEqualTo("express");

		infoMsg.setHeader(IntegrationMessageHeaderAccessor.CORRELATION_ID, "1000-0000-0000");

		infoNeedPartQueryInputChannel.send(infoMsg.build());

		Thread.sleep(5000);

	}
}
