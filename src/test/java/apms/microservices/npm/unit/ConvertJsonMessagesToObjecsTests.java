package apms.microservices.npm.unit;

import static apms.transport.utils.JSONUtils.asJSON;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import apms.microservices.npm.config.NpmMicroserviceConfig;
import apms.microservices.npm.domain.NpmGetCmd;
import apms.microservices.npm.service.NpmMsgsTransformer;

@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationComponentScan
@ComponentScan(basePackageClasses = { NpmMicroserviceConfig.class })
@SuppressWarnings("serial")
public class ConvertJsonMessagesToObjecsTests {

	@Autowired(required = true)
	private NpmMsgsTransformer transformer;

	@Test
	public void it_converts_a_json_msg_to_an_object_msg() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "npm");
				put("cmd", "get");
				put("name", "express");
			}
		};

		Message<?> result = transformer.transformCmdGetJSONToObject(MessageBuilder.withPayload(asJSON(msg)).build());
		NpmGetCmd SAMPLE_CONTENT = new NpmGetCmd("express");

		assertThat(result.getPayload()).isEqualTo(SAMPLE_CONTENT);

	}
}
