package apms.microservices.npm.unit;

import static apms.transport.utils.JSONUtils.asJSON;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import apms.microservices.npm.config.NpmMicroserviceConfig;
import apms.microservices.npm.service.NpmRouter;
import apms.transport.TransportComponentScanEntryPoint;

@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationComponentScan
@ComponentScan(basePackageClasses = { NpmMicroserviceConfig.class, TransportComponentScanEntryPoint.class })
@SuppressWarnings("serial")
public class RoutingTests {

	@Autowired
	private NpmRouter matcher;

	@Test
	public void it_handles_npm_get_msgs() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "npm");
				put("cmd", "get");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("npmCmdGetInputChannel");
	}

	@Test
	public void it_handles_npm_query_msgs() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "npm");
				put("cmd", "query");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("npmCmdQueryInputChannel");
	}

	@Test
	public void it_handles_info_need_part_query_msgs() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "info");
				put("need", "part");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("infoNeedPartQueryInputChannel");
	}

	@Test
	public void it_handles_extract_query_msgs() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "npm");
				put("cmd", "extract");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("npmCmdExtractInputChannel");
	}

	@Test
	public void it_rejects_other_msgs() {
		Map<String, String> msg = new HashMap<String, String>() {
			{
				put("role", "npm");
				put("cmd", "info");
			}
		};

		assertThat(matcher.route(asJSON(msg))).contains("withoutDestination");
	}
}
