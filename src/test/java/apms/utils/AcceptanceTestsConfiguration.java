package apms.utils;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("apms.microservices.git.steps")
public class AcceptanceTestsConfiguration {

}
