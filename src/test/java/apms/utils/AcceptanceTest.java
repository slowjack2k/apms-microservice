package apms.utils;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = AcceptanceTestsConfiguration.class)
@ActiveProfiles({ "test", "acceptance" })
@DirtiesContext
@Retention(RUNTIME)
@Target(TYPE)
public @interface AcceptanceTest {

}
