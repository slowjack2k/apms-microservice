package apms.microservices.info.domain;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash(value = "InfoData", timeToLive = 600)
public class ModuleInfos {
	@Id
	private String name;
	private Map<String, String> data;

	protected ModuleInfos() {
		super();
	}

	public ModuleInfos(String name, Map<String, String> data) {
		super();
		this.name = name;
		this.data = data;
	}

	public Map<String, String> getData() {
		return new HashMap<>(data);
	}

	@Override
	public String toString() {
		return "ModuleInfos [name=" + name + ", data=" + data + "]";
	}

}
