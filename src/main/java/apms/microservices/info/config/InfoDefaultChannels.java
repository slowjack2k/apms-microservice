package apms.microservices.info.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.AbstractMessageChannel;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;

import apms.transport.utils.StringConverter;

@Configuration
public class InfoDefaultChannels {

	@Bean("infoRoutingInputChannel")
	public MessageChannel infoRoutingInputChannel() {
		AbstractMessageChannel result = new DirectChannel();
		result.setDatatypes(String.class);
		result.setMessageConverter(new StringConverter());

		return result;
	}

}
