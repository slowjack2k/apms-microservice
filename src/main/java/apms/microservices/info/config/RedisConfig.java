package apms.microservices.info.config;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
public class RedisConfig {
	@Value("${rediscloud.url}")
	private String redisCloudUrl;

	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		JedisConnectionFactory jedisConFactory = new JedisConnectionFactory();

		if (null != redisCloudUrl && !redisCloudUrl.isEmpty()) {
			URI uri = null;
			try {
				uri = new URI(redisCloudUrl);
			} catch (URISyntaxException e) {

			}
			jedisConFactory.setHostName(uri.getHost());
			jedisConFactory.setPort(uri.getPort());
			jedisConFactory.setPort(uri.getPort());

			if (uri.getUserInfo() != null) {
				jedisConFactory.setPassword(uri.getUserInfo().split(":", 2)[1]);
			}
		}

		return jedisConFactory;
	}

	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		RedisTemplate<String, Object> template = new RedisTemplate<>();
		template.setConnectionFactory(jedisConnectionFactory());
		return template;
	}
}
