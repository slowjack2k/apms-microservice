package apms.microservices.info.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAutoConfiguration
@EnableRedisRepositories("apms.microservices.info.domain")
@ComponentScan("apms.microservices.info")
@EnableIntegration
@IntegrationComponentScan("apms.microservices.info")
public class InfoMicroserviceConfig {
	@Bean("infoGetThreadPoolTaskExecutor")
	public TaskExecutor infoGetThreadPoolTaskExecutor() {

		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setThreadNamePrefix("info-get-");
		executor.initialize();

		return executor;
	}

	@Bean("infoCollectThreadPoolTaskExecutor")
	public TaskExecutor infoCollectThreadPoolTaskExecutor() {

		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setThreadNamePrefix("info-collect-");
		executor.initialize();

		return executor;
	}
}
