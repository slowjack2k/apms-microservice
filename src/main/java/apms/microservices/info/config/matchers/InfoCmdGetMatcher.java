package apms.microservices.info.config.matchers;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import apms.transport.RoutingMatcher;

@Component
public class InfoCmdGetMatcher implements RoutingMatcher {
	@Autowired(required = true)
	@Qualifier("infoCmdGetInputChannel")
	private MessageChannel ensureChannelWithNameExists;

	@Override
	public int cntMatches(Map<String, String> msg) {
		boolean role = Optional.ofNullable(msg.get("role")).map((value) -> value.equals("info")).orElse(false);
		boolean cmd = Optional.ofNullable(msg.get("cmd")).map((value) -> value.equals("get")).orElse(false);

		if (role && cmd) {
			return 2;
		} else {
			return 0;
		}
	}

	@Override
	public String group() {
		return "info";
	}

	@Override
	public String channel() {
		// stick to name, not channel to decouple things
		return "infoCmdGetInputChannel";
	}
}