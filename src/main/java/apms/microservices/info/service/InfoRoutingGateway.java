package apms.microservices.info.service;

import java.util.Map;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

@MessagingGateway(name = "infoRoutingGateway", defaultRequestChannel = "infoRoutingInputChannel")
public interface InfoRoutingGateway {
	@Gateway(replyTimeout = 10000)
	public void sendNeedPart(Message<Map<String, String>> msg);
}
