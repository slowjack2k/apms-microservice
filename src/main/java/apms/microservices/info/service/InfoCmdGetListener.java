package apms.microservices.info.service;

import static apms.transport.utils.JSONUtils.asJSON;
import static apms.transport.utils.MessageHelper.buildMessage;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

import apms.microservices.info.domain.DataRepository;
import apms.microservices.info.domain.ModuleInfos;

@MessageEndpoint
public class InfoCmdGetListener {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private InfoRoutingGateway gateway;
	private DataRepository repo;

	@Autowired
	public InfoCmdGetListener(InfoRoutingGateway gateway, DataRepository repo) {
		super();
		this.repo = repo;
		this.gateway = gateway;
	}

	@SuppressWarnings("serial")
	@ServiceActivator(inputChannel = "execInfoCmdGetChannel")
	public Message<String> handleCmdGet(@Payload Map<String, String> cmd, @Headers MessageHeaders headers) {
		final String name = cmd.get("name");

		Map<String, String> newCmd = new HashMap<String, String>() {
			{

				{
					put("role", "info");
					put("need", "part");
					put("name", name);
				}
			}
		};

		gateway.sendNeedPart(buildMessage(newCmd, headers));

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {

		}

		ModuleInfos data = repo.findById(name).orElseGet(() -> new ModuleInfos(cmd.get("name"), new HashMap<>()));

		logger.info("Data: " + data);

		String json = asJSON(data);

		if (headers.containsKey("replyChannel")) {
			return buildMessage(json, headers);
		} else {
			return null;
		}
	}
}
