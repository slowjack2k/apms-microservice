package apms.microservices.info.service;

import static apms.transport.utils.JSONUtils.JSONtoMap;

import java.util.Map;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

@MessageEndpoint
public class InfoMsgsTransformer {

	@Transformer(inputChannel = "infoCmdGetInputChannel", outputChannel = "infoCmdGetTransformedOutputChannel")
	public Message<Map<String, String>> transformCmdGetJSONToObject(Message<String> msg) {
		Map<String, String> data = JSONtoMap(msg.getPayload());

		return MessageBuilder.withPayload(data).copyHeadersIfAbsent(msg.getHeaders()).build();
	}

	@Transformer(inputChannel = "infoCmdCollectInputChannel", outputChannel = "infoCmdCollectTransformedOutputChannel")
	public Message<Map<String, String>> transformCmdCollectJSONToObject(Message<String> msg) {
		Map<String, String> data = JSONtoMap(msg.getPayload());

		return MessageBuilder.withPayload(data).copyHeadersIfAbsent(msg.getHeaders()).build();
	}
}
