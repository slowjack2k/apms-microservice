package apms.microservices.info.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

import apms.microservices.info.domain.DataRepository;
import apms.microservices.info.domain.ModuleInfos;

@MessageEndpoint
public class InfoCmdCollectListener {
	private DataRepository repo;

	@Autowired
	public InfoCmdCollectListener(DataRepository repo) {
		super();
		this.repo = repo;
	}

	@ServiceActivator(inputChannel = "execInfoCmdCollectChannel")
	public void handleCmdCollect(@Payload Map<String, String> cmd, @Headers MessageHeaders headers) {
		final String name = cmd.get("name");
		ModuleInfos moduleInfos = repo.findById(name)
				.orElseGet(() -> new ModuleInfos(cmd.get("name"), new HashMap<>()));
		Map<String, String> data = moduleInfos.getData();

		data.put(cmd.get("part"), cmd.get("data"));

		moduleInfos = new ModuleInfos(cmd.get("name"), data);

		repo.save(moduleInfos);

	}
}
