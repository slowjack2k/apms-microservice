package apms.microservices.info.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Router;

import apms.transport.MsgMatchingRouter;

@MessageEndpoint
public class InfoRouter {
	private MsgMatchingRouter matcher;

	@Autowired
	public InfoRouter(MsgMatchingRouter matcher) {
		super();
		this.matcher = matcher;
	}

	@Router(inputChannel = "infoRoutingInputChannel")
	public Collection<String> route(String json) {
		return matcher.route(json);
	}
}
