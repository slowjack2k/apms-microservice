package apms.microservices.git.service;

import static apms.transport.utils.MessageHelper.buildMessage;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.node.ObjectNode;

@MessageEndpoint
public class GitCmdQueryListener {
	protected GitRoutingGateway gateway;

	protected RestTemplate restTemplate;

	@Autowired
	public GitCmdQueryListener(GitRoutingGateway gateway, RestTemplate restTemplate) {
		super();
		this.gateway = gateway;
		this.restTemplate = restTemplate;
	}

	@SuppressWarnings("serial")
	@ServiceActivator(inputChannel = "execGitCmdQueryChannel", outputChannel = "msgToJsonConverterChannel")
	public Message<String> handleCmdQuery(@Payload Map<String, String> cmd, @Headers Map<String, Object> headers) {
		String result = "";

		try {
			ObjectNode json = restTemplate.getForObject(
					"https://api.github.com/repos/" + cmd.get("owner") + "/" + cmd.get("repo"), ObjectNode.class);
			result = json.toString();

		} catch (HttpClientErrorException e) {

		}

		Map<String, String> newCmd = new HashMap<String, String>() {
			{

				{
					put("role", "git");
					put("cmd", "extract");
					put("owner", cmd.get("owner"));
					put("repo", cmd.get("repo"));
				}
			}
		};

		newCmd.put("data", result.toString());

		return gateway.sendExtract(buildMessage(newCmd, headers));

	}
}
