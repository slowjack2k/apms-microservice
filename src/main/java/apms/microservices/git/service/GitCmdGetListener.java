package apms.microservices.git.service;

import static apms.transport.utils.JSONUtils.JSONtoMap;
import static apms.transport.utils.MessageHelper.buildMessage;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

import apms.microservices.git.domain.NpmModuleGitRepository;
import apms.microservices.git.domain.entities.NpmModuleGit;

@MessageEndpoint
public class GitCmdGetListener {
	public static final Pattern GIT_URL_PATTERN = Pattern.compile("[\\/:]([^\\/:]+?)[\\/:]([^\\/]+?)(\\.git)*$");
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private NpmModuleGitRepository repo;
	private GitRoutingGateway gateway;

	@Autowired
	public GitCmdGetListener(NpmModuleGitRepository repo, GitRoutingGateway gateway) {
		super();
		this.repo = repo;
		this.gateway = gateway;
	}

	@SuppressWarnings("serial")
	@ServiceActivator(inputChannel = "execGitCmdGetChannel", outputChannel = "msgToJsonConverterChannel")
	public Message<String> handleCmdQuery(@Payload Map<String, String> cmd, @Headers Map<String, Object> headers) {
		Optional<NpmModuleGit> module = repo.findByName(cmd.get("name"));
		if (!module.isPresent()) {
			Map<String, String> newCmd = new HashMap<String, String>() {
				{

					{
						put("role", "npm");
						put("cmd", "get");
						put("name", cmd.get("name"));
					}
				}
			};

			Message<String> result = gateway.sendNpmQuery(buildMessage(newCmd, headers));

			if (result == null) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {

				}

				result = gateway.sendNpmQuery(buildMessage(newCmd, headers));
			}

			String json = "{}";

			if (result != null) {
				json = result.getPayload();
			}

			Map<String, String> data = JSONtoMap(json);
			String gitUrl = data.get("giturl");

			Map<String, String> newGetCmd = new HashMap<String, String>() {
				{

					{
						put("role", "git");
						put("cmd", "get");
						put("name", cmd.get("name"));
						put("giturl", gitUrl);
					}
				}
			};

			return gateway.sendGet(buildMessage(newGetCmd, headers));

		} else {

			return buildMessage(module.get().getData(), headers);
		}
	}

	@SuppressWarnings("serial")
	@ServiceActivator(inputChannel = "gitCmdGetWithUrlInputChannel", outputChannel = "msgToJsonConverterChannel")
	public Message<String> handleCmdQueryWithUrl(@Payload Map<String, String> cmd,
			@Headers Map<String, Object> headers) {
		Message<String> result = buildMessage("{}", headers);
		Matcher m = GIT_URL_PATTERN.matcher(cmd.get("giturl"));

		if (m.find()) {
			Map<String, String> newCmd = new HashMap<String, String>() {
				{

					{
						put("role", "git");
						put("cmd", "query");
						put("name", cmd.get("name"));
						put("owner", m.group(1));
						put("repo", m.group(2));

					}
				}
			};

			result = gateway.sendQuery(buildMessage(newCmd, headers));
		}
		NpmModuleGit newModule = new NpmModuleGit(cmd.get("name"), result.getPayload());

		repo.save(newModule);

		return buildMessage(newModule.getData(), headers);
	}
}
