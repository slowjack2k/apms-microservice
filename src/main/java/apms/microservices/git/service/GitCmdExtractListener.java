package apms.microservices.git.service;

import static apms.transport.utils.JSONUtils.asJSON;
import static apms.transport.utils.MessageHelper.buildMessage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@MessageEndpoint
public class GitCmdExtractListener {
	private final static ObjectMapper mapper = new ObjectMapper();
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ServiceActivator(inputChannel = "execGitCmdExtractChannel", outputChannel = "msgToJsonConverterChannel")
	public Message<String> handleCmdQuery(@Payload Map<String, String> cmd, @Headers Map<String, Object> headers) {
		HashMap<String, String> result = new HashMap<String, String>();

		logger.info(cmd.get("data"));

		try {
			JsonNode data = mapper.readTree(cmd.get("data"));

			if (data == null) {
				data = mapper.createObjectNode();
			}

			JsonNode stargazers_count = data.path("stargazers_count");
			JsonNode subscribers_count = data.path("subscribers_count");
			JsonNode forks_count = data.path("forks_count");
			JsonNode pushed_at = data.path("pushed_at");

			result.put("owner", cmd.get("owner"));
			result.put("repo", cmd.get("repo"));
			result.put("stars", stargazers_count.asText());
			result.put("watchers", subscribers_count.asText());
			result.put("forks", forks_count.asText());
			result.put("last", pushed_at.asText());

		} catch (IOException e) {
			logger.error("Error: ", e);

		}

		return buildMessage(asJSON(result), headers);
	}

	@ServiceActivator(inputChannel = "msgToJsonConverterChannel")
	public Message<?> doNothing(Message<?> msg) {

		return msg;
	}
}
