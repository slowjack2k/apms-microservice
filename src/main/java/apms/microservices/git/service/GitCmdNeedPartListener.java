package apms.microservices.git.service;

import static apms.transport.utils.MessageHelper.buildMessage;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

@MessageEndpoint
public class GitCmdNeedPartListener {
	private GitRoutingGateway gateway;

	@Autowired
	public GitCmdNeedPartListener(GitRoutingGateway gateway) {
		super();
		this.gateway = gateway;
	}

	@SuppressWarnings("serial")
	@ServiceActivator(inputChannel = "execGitInfoNeedPartQueryChannel")
	public void handleNeedPart(@Payload Map<String, String> infoCmd, @Headers MessageHeaders headers) {
		Map<String, String> newCmd = new HashMap<String, String>() {
			{

				{
					put("role", "git");
					put("cmd", "get");
					put("name", infoCmd.get("name"));
				}
			}
		};

		Map<String, String> collectCmd = new HashMap<String, String>() {
			{

				{
					put("role", "info");
					put("collect", "part");
					put("part", "git");
					put("name", infoCmd.get("name"));
				}
			}
		};

		Message<String> r = gateway.sendGet(buildMessage(newCmd, headers));

		if (r != null && r.getPayload() != null) {
			collectCmd.put("data", r.getPayload());
			gateway.sendCollect(buildMessage(collectCmd, headers));
		}

	}
}
