package apms.microservices.git.service;

import java.util.Map;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

@MessagingGateway(name = "gitRoutingGateway", defaultRequestChannel = "gitRoutingOutputChannel")
public interface GitRoutingGateway {
	@Gateway(replyTimeout = 10000, replyChannel = "jsonToMapConverterChannel")
	public Message<String> sendQuery(Message<Map<String, String>> msg);

	@Gateway(replyTimeout = 10000, replyChannel = "jsonToMapConverterChannel")
	public Message<String> sendNpmQuery(Message<Map<String, String>> msg);

	@Gateway(replyTimeout = 10000, replyChannel = "jsonToMapConverterChannel")
	public Message<String> sendExtract(Message<Map<String, String>> msg);

	@Gateway(replyTimeout = 10000, replyChannel = "jsonToMapConverterChannel")
	public Message<String> sendGet(Message<Map<String, String>> msg);

	@Gateway(replyTimeout = 10000, replyChannel = "jsonToMapConverterChannel")
	public void sendCollect(Message<Map<String, String>> msg);
}
