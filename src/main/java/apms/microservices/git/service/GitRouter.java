package apms.microservices.git.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Router;

import apms.transport.MsgMatchingRouter;

@MessageEndpoint
public class GitRouter {
	private MsgMatchingRouter matcher;

	@Autowired
	public GitRouter(MsgMatchingRouter matcher) {
		super();
		this.matcher = matcher;
	}

	@Router(inputChannel = "gitRoutingJSONOutputChannel")
	public Collection<String> route(String json) {
		return matcher.route(json);
	}
}
