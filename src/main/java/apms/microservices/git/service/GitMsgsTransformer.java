package apms.microservices.git.service;

import static apms.transport.utils.JSONUtils.JSONtoMap;
import static apms.transport.utils.JSONUtils.objMsgToJSONMsg;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

@MessageEndpoint
public class GitMsgsTransformer {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Transformer(inputChannel = "gitCmdGetInputChannel", outputChannel = "gitCmdGetTransformedOutputChannel")
	public Message<Map<String, String>> transformGitCmdGetJSONToObject(Message<String> msg) {
		Map<String, String> data = JSONtoMap(msg.getPayload());

		return MessageBuilder.withPayload(data).copyHeadersIfAbsent(msg.getHeaders()).build();
	}

	@Transformer(inputChannel = "gitCmdQueryInputChannel", outputChannel = "gitCmdQueryTransformedOutputChannel")
	public Message<Map<String, String>> transformGitCmdQueryJSONToObject(Message<String> msg) {
		Map<String, String> data = JSONtoMap(msg.getPayload());

		return MessageBuilder.withPayload(data).copyHeadersIfAbsent(msg.getHeaders()).build();
	}

	@Transformer(inputChannel = "gitInfoNeedPartQueryInputChannel", outputChannel = "gitInfoNeedPartQueryTransformedOutputChannel")
	public Message<Map<String, String>> transformGitNeedPartJSONToObject(Message<String> msg) {
		Map<String, String> data = JSONtoMap(msg.getPayload());

		return MessageBuilder.withPayload(data).copyHeadersIfAbsent(msg.getHeaders()).build();
	}

	@Transformer(inputChannel = "gitCmdExtractInputChannel", outputChannel = "gitCmdExtractTransformedOutputChannel")
	public Message<Map<String, String>> transformGitCmdExtractJSONToObject(Message<String> msg) {
		Map<String, String> data = JSONtoMap(msg.getPayload());

		return MessageBuilder.withPayload(data).copyHeadersIfAbsent(msg.getHeaders()).build();
	}

	@Transformer(inputChannel = "gitRoutingOutputChannel", outputChannel = "gitRoutingJSONOutputChannel")
	public Message<String> transformGitRoutingObjectToJSON(Message<?> msg) {
		return objMsgToJSONMsg(msg);
	}
}
