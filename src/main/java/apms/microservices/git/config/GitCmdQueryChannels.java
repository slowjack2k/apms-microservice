package apms.microservices.git.config;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.BridgeFrom;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.MessageChannel;

@Configuration
public class GitCmdQueryChannels {

	@Bean
	public MessageChannel gitCmdQueryInputChannel() {
		return new DirectChannel();
	}

	@Bean
	public QueueChannel gitCmdQueryTransformedOutputChannel() {
		return new QueueChannel();
	}

	@Bean
	@Autowired
	@BridgeFrom(value = "gitCmdQueryTransformedOutputChannel", poller = @Poller(fixedDelay = "100", maxMessagesPerPoll = "1"))
	public ExecutorChannel execGitCmdQueryChannel(@Qualifier("gitQueryThreadPoolTaskExecutor") Executor taskExecutor) {
		return new ExecutorChannel(taskExecutor);
	}

}
