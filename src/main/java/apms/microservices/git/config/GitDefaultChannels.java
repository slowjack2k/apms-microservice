package apms.microservices.git.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;

@Configuration
public class GitDefaultChannels {

	@Bean
	public MessageChannel gitRoutingOutputChannel() {
		return new DirectChannel();
	}

	@Bean
	public MessageChannel gitRoutingJSONOutputChannel() {
		return new DirectChannel();
	}
}
