package apms.microservices.git.config;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import apms.transport.RoutingMatcher;

@Component
public class GitMessagePatterns {
	@Component
	public static class GitCmdGetMatcher implements RoutingMatcher {
		@Autowired(required = true)
		@Qualifier("gitCmdGetInputChannel")
		private MessageChannel ensureChannelWithNameExists;

		@Override
		public int cntMatches(Map<String, String> msg) {
			boolean role = Optional.ofNullable(msg.get("role")).map((value) -> value.equals("git")).orElse(false);
			boolean cmd = Optional.ofNullable(msg.get("cmd")).map((value) -> value.equals("get")).orElse(false);

			if (role && cmd) {
				return 2;
			} else {
				return 0;
			}
		}

		@Override
		public String group() {
			return "git";
		}

		@Override
		public String channel() {
			// stick to name, not channel to decouple things
			return "gitCmdGetInputChannel";
		}
	}

	@Component
	public static class GitCmdGetWithUrlMatcher implements RoutingMatcher {
		@Autowired(required = true)
		@Qualifier("gitCmdGetWithUrlInputChannel")
		private MessageChannel ensureChannelWithNameExists;

		@Override
		public int cntMatches(Map<String, String> msg) {
			boolean role = Optional.ofNullable(msg.get("role")).map((value) -> value.equals("git")).orElse(false);
			boolean cmd = Optional.ofNullable(msg.get("cmd")).map((value) -> value.equals("get")).orElse(false);
			boolean gitUrl = msg.containsKey("giturl");

			if (role && cmd && gitUrl) {
				return 3;
			} else {
				return 0;
			}
		}

		@Override
		public String group() {
			return "git";
		}

		@Override
		public String channel() {
			// stick to name, not channel to decouple things
			return "gitCmdGetWithUrlInputChannel";
		}
	}

	@Component
	public static class GitCmdQueryMatcher implements RoutingMatcher {
		@Autowired(required = true)
		@Qualifier("gitCmdQueryInputChannel")
		private MessageChannel ensureChannelWithNameExists;

		@Override
		public int cntMatches(Map<String, String> msg) {
			boolean role = Optional.ofNullable(msg.get("role")).map((value) -> value.equals("git")).orElse(false);
			boolean cmd = Optional.ofNullable(msg.get("cmd")).map((value) -> value.equals("query")).orElse(false);

			if (role && cmd) {
				return 2;
			} else {
				return 0;
			}
		}

		@Override
		public String group() {
			return "git";
		}

		@Override
		public String channel() {
			// stick to name, not channel to decouple things
			return "gitCmdQueryInputChannel";
		}
	}

	@Component
	public static class GitCmdExtractMatcher implements RoutingMatcher {
		@Autowired(required = true)
		@Qualifier("gitCmdExtractInputChannel")
		private MessageChannel ensureChannelWithNameExists;

		@Override
		public int cntMatches(Map<String, String> msg) {
			boolean role = Optional.ofNullable(msg.get("role")).map((value) -> value.equals("git")).orElse(false);
			boolean cmd = Optional.ofNullable(msg.get("cmd")).map((value) -> value.equals("extract")).orElse(false);

			if (role && cmd) {
				return 2;
			} else {
				return 0;
			}
		}

		@Override
		public String group() {
			return "git";
		}

		@Override
		public String channel() {
			// stick to name, not channel to decouple things
			return "gitCmdExtractInputChannel";
		}
	}

	@Component
	public static class InfoNeedPartQueryMatcher implements RoutingMatcher {
		@Autowired(required = true)
		@Qualifier("gitInfoNeedPartQueryInputChannel")
		private MessageChannel ensureChannelWithNameExists;

		@Override
		public int cntMatches(Map<String, String> msg) {
			boolean role = Optional.ofNullable(msg.get("role")).map((value) -> value.equals("info")).orElse(false);
			boolean cmd = Optional.ofNullable(msg.get("need")).map((value) -> value.equals("part")).orElse(false);

			if (role && cmd) {
				return 2;
			} else {
				return 0;
			}
		}

		@Override
		public String group() {
			return "git";
		}

		@Override
		public String channel() {
			// stick to name, not channel to decouple things
			return "gitInfoNeedPartQueryInputChannel";
		}
	}
}
