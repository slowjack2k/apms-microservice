package apms.microservices.git.config;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.BridgeFrom;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.MessageChannel;

@Configuration
public class GitCmdGetChannels {

	@Bean
	public MessageChannel gitCmdGetInputChannel() {
		return new DirectChannel();
	}

	@Bean
	public QueueChannel gitCmdGetTransformedOutputChannel() {
		return new QueueChannel();
	}

	@Bean
	@Autowired
	@BridgeFrom(value = "gitCmdGetTransformedOutputChannel", poller = @Poller(fixedDelay = "100", maxMessagesPerPoll = "1"))
	public ExecutorChannel execGitCmdGetChannel(@Qualifier("gitGetThreadPoolTaskExecutor") Executor taskExecutor) {
		return new ExecutorChannel(taskExecutor);
	}

	@Bean
	public MessageChannel gitCmdGetWithUrlInputChannel() {
		return new DirectChannel();
	}

	@Bean
	public QueueChannel gitCmdGetWithUrlTransformedOutputChannel() {
		return new QueueChannel();
	}

	@Bean
	@Autowired
	@BridgeFrom(value = "gitCmdGetWithUrlTransformedOutputChannel", poller = @Poller(fixedDelay = "100", maxMessagesPerPoll = "1"))
	public ExecutorChannel execGitCmdGetWithUrlChannel(
			@Qualifier("gitGetThreadPoolTaskExecutor") Executor taskExecutor) {
		return new ExecutorChannel(taskExecutor);
	}

}
