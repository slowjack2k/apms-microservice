package apms.microservices.git.domain.entities;

import static apms.transport.utils.JSONUtils.JSONtoMap;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(indexes = { @Index(name = "npm_module_name_git", columnList = "NAME", unique = true) })
public class NpmModuleGit {
	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	@Lob
	private String data;

	protected NpmModuleGit() {
	}

	public NpmModuleGit(String name, String data) {
		super();
		this.name = name;
		this.data = data;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Long getId() {
		return id;
	}

	public Map<String, String> getDataAsMap() {
		return JSONtoMap(getData());
	}

	@Override
	public String toString() {
		return "NpmModuleGit [id=" + id + ", name=" + name + ", data=" + data + "]";
	}

}
