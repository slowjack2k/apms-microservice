package apms.microservices.git.domain;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import apms.microservices.git.domain.entities.NpmModuleGit;

@Repository
@Transactional
public interface NpmModuleGitRepository extends JpaRepository<NpmModuleGit, Long> {

	Optional<NpmModuleGit> findByName(String name);

}
