package apms.microservices.web.config;

import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.integration.channel.AbstractMessageChannel;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.messaging.MessageChannel;

import apms.transport.utils.MapConverter;
import apms.transport.utils.StringConverter;

@Configuration
public class WebDefaultChannels {
	protected static final ExpressionParser EXPRESSION_PARSER = new SpelExpressionParser();

	@Bean("webRoutingOutputChannel")
	public MessageChannel webRoutingOutputChannel() {
		AbstractMessageChannel channel = new DirectChannel();
		channel.setDatatypes(String.class);
		channel.setMessageConverter(new StringConverter());
		return channel;
	}

	@Bean
	public MessageChannel jsonToMapConverterChannel2() {
		AbstractMessageChannel result = new PublishSubscribeChannel();
		result.setDatatypes(Map.class);
		result.setMessageConverter(new MapConverter(true));
		return result;
	}
}
