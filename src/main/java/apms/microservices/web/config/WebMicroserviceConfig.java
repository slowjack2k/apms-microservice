package apms.microservices.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration

@ComponentScan("apms.microservices.web")
@EnableIntegration
@IntegrationComponentScan("apms.microservices.web")
public class WebMicroserviceConfig {
	@Bean("npmGetThreadPoolTaskExecutor")
	public TaskExecutor npmGetThreadPoolTaskExecutor() {

		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setThreadNamePrefix("npm-get-");
		executor.initialize();

		return executor;
	}

	@Bean("npmQueryThreadPoolTaskExecutor")
	public TaskExecutor npmQueryThreadPoolTaskExecutor() {

		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setThreadNamePrefix("npm-query-");
		executor.initialize();

		return executor;
	}

	@Bean("npmInfoThreadPoolTaskExecutor")
	public TaskExecutor npmInfoThreadPoolTaskExecutor() {

		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setThreadNamePrefix("npm-info-");
		executor.initialize();

		return executor;
	}
}