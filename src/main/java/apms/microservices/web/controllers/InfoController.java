package apms.microservices.web.controllers;

import static apms.transport.utils.JSONUtils.JSONtoMap;
import static apms.transport.utils.JSONUtils.JSONtoTree;
import static apms.transport.utils.MessageHelper.buildMessage;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;

import apms.microservices.web.domain.responses.InfoResponePartGit;
import apms.microservices.web.domain.responses.InfoResponePartNpm;
import apms.microservices.web.domain.responses.InfoResponse;
import apms.microservices.web.service.WebRoutingGateway;

@RestController
@RequestMapping("/api/info")
public class InfoController {
	private final static Logger LOG = LoggerFactory.getLogger(InfoController.class);
	private WebRoutingGateway gateway;

	@Autowired
	public InfoController(WebRoutingGateway gateway) {
		super();

		this.gateway = gateway;
	}

	@PostMapping(value = "/{module-name}")
	public String sample(@PathVariable("module-name") String name, @RequestBody String json) {
		LOG.error("JSON: " + json);
		return json;
	}

	@GetMapping(value = "/{module-name}")
	public InfoResponse show(@PathVariable("module-name") String name) {
		Map<String, String> cmd = new HashMap<String, String>() {
			{

				put("role", "info");
				put("cmd", "get");
				put("name", name);

				/*
				 * 
				 * { put("role", "info"); put("need", "part"); put("name", "express"); }
				 */
				/*
				 * { put("role", "npm"); put("cmd", "get"); put("name", name); }
				 */
			}
		};

		Message<Map<String, String>> infoMsg = buildMessage(cmd);

		Message<String> result = gateway.sendGet(infoMsg);
		JsonNode data = JSONtoTree("{}");
		if (result != null) {
			data = JSONtoTree(result.getPayload());
		}

		InfoResponePartNpm npm = null;
		InfoResponePartGit git = null;

		if (!data.path("data").path("npm").isMissingNode()) {
			Map<String, String> dataNpm = JSONtoMap(data.path("data").path("npm").asText());

			npm = new InfoResponePartNpm(dataNpm.get("name"), dataNpm.get("version"), dataNpm.get("giturl"),
					dataNpm.get("desc"), dataNpm.get("readme"));
		}

		if (!data.path("data").path("git").isMissingNode()) {
			Map<String, String> dataGit = JSONtoMap(data.path("data").path("git").asText());
			git = new InfoResponePartGit(dataGit.get("owner"), dataGit.get("repo"), dataGit.get("stars"),
					dataGit.get("watchers"), dataGit.get("forks"), dataGit.get("last"));
		}

		return new InfoResponse(npm, git);
	}
}
