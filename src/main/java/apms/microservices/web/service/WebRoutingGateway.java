package apms.microservices.web.service;

import java.util.Map;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

@MessagingGateway(name = "webRoutingGateway", defaultRequestChannel = "webRoutingOutputChannel")
public interface WebRoutingGateway {
	@Gateway(replyTimeout = 10000, replyChannel = "jsonToMapConverterChannel2")
	public Message<String> sendGet(Message<Map<String, String>> msg);
}
