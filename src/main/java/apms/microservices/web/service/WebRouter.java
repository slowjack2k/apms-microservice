package apms.microservices.web.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Router;

import apms.transport.MsgMatchingRouter;

@MessageEndpoint
public class WebRouter {
	private MsgMatchingRouter matcher;

	@Autowired
	public WebRouter(MsgMatchingRouter matcher) {
		super();
		this.matcher = matcher;
	}

	@Router(inputChannel = "webRoutingOutputChannel")
	public Collection<String> route(String json) {
		return matcher.route(json);
	}
}
