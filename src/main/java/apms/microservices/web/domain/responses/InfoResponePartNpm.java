package apms.microservices.web.domain.responses;

public class InfoResponePartNpm {

	protected String name;
	protected String version;
	protected String giturl;
	protected String desc;
	protected String readme;

	public InfoResponePartNpm(String name, String version, String giturl, String desc, String readme) {
		super();
		this.name = name;
		this.version = version;
		this.giturl = giturl;
		this.desc = desc;
		this.readme = readme;
	}

	public String getName() {
		return name;
	}

	public String getVersion() {
		return version;
	}

	public String getGiturl() {
		return giturl;
	}

	public String getDesc() {
		return desc;
	}

	public String getReadme() {
		return readme;
	}

	@Override
	public String toString() {
		return "InfoResponePartNpm [name=" + name + ", version=" + version + ", giturl=" + giturl + "]";
	}
}
