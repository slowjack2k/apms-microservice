package apms.microservices.web.domain.responses;

public class InfoResponse {

	protected InfoResponePartNpm npm;
	protected InfoResponePartGit git;

	public InfoResponse(InfoResponePartNpm npm, InfoResponePartGit git) {
		super();
		this.npm = npm;
		this.git = git;
	}

	public InfoResponePartNpm getNpm() {
		return npm;
	}

	public InfoResponePartGit getGit() {
		return git;
	}

}
