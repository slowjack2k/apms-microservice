package apms.microservices.web.domain.responses;

public class InfoResponePartGit {

	protected String owner;
	protected String repo;
	protected String stars;
	protected String watchers;
	protected String forks;
	protected String last;

	public InfoResponePartGit(String owner, String repo, String stars, String watchers, String forks, String last) {
		super();
		this.owner = owner;
		this.repo = repo;
		this.stars = stars;
		this.watchers = watchers;
		this.forks = forks;
		this.last = last;
	}

	public String getOwner() {
		return owner;
	}

	public String getRepo() {
		return repo;
	}

	public String getStars() {
		return stars;
	}

	public String getWatchers() {
		return watchers;
	}

	public String getForks() {
		return forks;
	}

	public String getLast() {
		return last;
	}

	@Override
	public String toString() {
		return "InfoResponePartGit [owner=" + owner + ", repo=" + repo + ", stars=" + stars + ", watchers=" + watchers
				+ ", forks=" + forks + ", last=" + last + "]";
	}
}
