package apms.microservices.npm.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAutoConfiguration
@ComponentScan("apms.microservices.npm")
@EnableIntegration
@EnableJpaRepositories("apms.microservices.npm.domain")
@EntityScan("apms.microservices.npm.domain")
@IntegrationComponentScan("apms.microservices.npm")
public class NpmMicroserviceConfig {
	@Bean("npmGetThreadPoolTaskExecutor")
	public TaskExecutor npmGetThreadPoolTaskExecutor() {

		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setThreadNamePrefix("npm-get-");
		executor.initialize();

		return executor;
	}

	@Bean("npmQueryThreadPoolTaskExecutor")
	public TaskExecutor npmQueryThreadPoolTaskExecutor() {

		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setThreadNamePrefix("npm-query-");
		executor.initialize();

		return executor;
	}

	@Bean("npmInfoThreadPoolTaskExecutor")
	public TaskExecutor npmInfoThreadPoolTaskExecutor() {

		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setThreadNamePrefix("npm-info-");
		executor.initialize();

		return executor;
	}
}
