package apms.microservices.npm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;

@Configuration
public class NpmDefaultChannels {

	@Bean("npmRoutingOutputChannel")
	public MessageChannel npmRoutingOutputChannel() {
		return new DirectChannel();
	}

	@Bean("npmRoutingJSONOutputChannel")
	public MessageChannel npmRoutingJSONOutputChannel() {
		return new DirectChannel();
	}
}
