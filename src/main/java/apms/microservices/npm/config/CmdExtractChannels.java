package apms.microservices.npm.config;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.BridgeFrom;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.MessageChannel;

@Configuration
public class CmdExtractChannels {

	@Bean
	public MessageChannel npmCmdExtractInputChannel() {
		return new DirectChannel();
	}

	@Bean
	public QueueChannel npmCmdExtractTransformedOutputChannel() {
		return new QueueChannel();
	}

	@Bean
	@Autowired
	@BridgeFrom(value = "npmCmdExtractTransformedOutputChannel", poller = @Poller(fixedDelay = "100", maxMessagesPerPoll = "1"))
	public ExecutorChannel execNpmCmdExtractChannel(
			@Qualifier("npmQueryThreadPoolTaskExecutor") Executor taskExecutor) {
		return new ExecutorChannel(taskExecutor);
	}

}
