package apms.microservices.npm.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import apms.microservices.npm.domain.entities.NpmModule;

@Repository
@Transactional
public interface NpmModuleRepository extends JpaRepository<NpmModule, Long> {

	NpmModule findByName(String name);

}
