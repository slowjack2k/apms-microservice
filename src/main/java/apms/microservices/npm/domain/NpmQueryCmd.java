package apms.microservices.npm.domain;

public class NpmQueryCmd {
	protected String role = "npm";
	protected String cmd = "query";
	protected String name;

	public NpmQueryCmd(String name) {
		super();
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public String getCmd() {
		return cmd;
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cmd == null) ? 0 : cmd.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NpmQueryCmd other = (NpmQueryCmd) obj;
		if (cmd == null) {
			if (other.cmd != null)
				return false;
		} else if (!cmd.equals(other.cmd))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NpmQueryCmd [role=" + role + ", cmd=" + cmd + ", name=" + name + "]";
	}

}
