package apms.microservices.npm.domain;

public class InfoCommand {
	protected String role = "info";
	protected String need = "part";
	protected String name;

	public InfoCommand(String name) {
		super();
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public String getNeed() {
		return need;
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((need == null) ? 0 : need.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InfoCommand other = (InfoCommand) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (need == null) {
			if (other.need != null)
				return false;
		} else if (!need.equals(other.need))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InfoCommand [role=" + role + ", need=" + need + ", name=" + name + "]";
	}

}
