package apms.microservices.npm.service;

import static apms.transport.utils.JSONUtils.JSONtoMap;
import static apms.transport.utils.JSONUtils.objMsgToJSONMsg;

import java.util.Map;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

import apms.microservices.npm.domain.InfoCommand;
import apms.microservices.npm.domain.NpmExtractCmd;
import apms.microservices.npm.domain.NpmGetCmd;
import apms.microservices.npm.domain.NpmQueryCmd;

@MessageEndpoint
public class NpmMsgsTransformer {

	@Transformer(inputChannel = "npmCmdGetInputChannel", outputChannel = "npmCmdGetTransformedOutputChannel")
	public Message<NpmGetCmd> transformCmdGetJSONToObject(Message<String> msg) {
		Map<String, String> data = JSONtoMap(msg.getPayload());

		return MessageBuilder.withPayload(new NpmGetCmd(data.get("name"))).copyHeadersIfAbsent(msg.getHeaders())
				.build();
	}

	@Transformer(inputChannel = "npmCmdQueryInputChannel", outputChannel = "npmCmdQueryTransformedOutputChannel")
	public Message<NpmQueryCmd> transformCmdQueryJSONToObject(Message<String> msg) {
		Map<String, String> data = JSONtoMap(msg.getPayload());

		return MessageBuilder.withPayload(new NpmQueryCmd(data.get("name"))).copyHeadersIfAbsent(msg.getHeaders())
				.build();
	}

	@Transformer(inputChannel = "infoNeedPartQueryInputChannel", outputChannel = "infoNeedPartQueryTransformedOutputChannel")
	public Message<InfoCommand> transformNeedPartJSONToObject(Message<String> msg) {
		Map<String, String> data = JSONtoMap(msg.getPayload());

		return MessageBuilder.withPayload(new InfoCommand(data.get("name"))).copyHeadersIfAbsent(msg.getHeaders())
				.build();
	}

	@Transformer(inputChannel = "npmCmdExtractInputChannel", outputChannel = "npmCmdExtractTransformedOutputChannel")
	public Message<NpmExtractCmd> transformCmdExtractJSONToObject(Message<String> msg) {
		Map<String, String> data = JSONtoMap(msg.getPayload());

		return MessageBuilder.withPayload(new NpmExtractCmd(data.get("name"), data.get("data")))
				.copyHeadersIfAbsent(msg.getHeaders()).build();
	}

	@Transformer(inputChannel = "npmRoutingOutputChannel", outputChannel = "npmRoutingJSONOutputChannel")
	public Message<String> transformRoutingObjectToJSON(Message<?> msg) {
		return objMsgToJSONMsg(msg);
	}
}
