package apms.microservices.npm.service;

import static apms.transport.utils.MessageHelper.buildMessage;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.node.ObjectNode;

import apms.microservices.npm.domain.NpmExtractCmd;
import apms.microservices.npm.domain.NpmQueryCmd;

@MessageEndpoint
public class CmdQueryListener {
	private RoutingGateway gateway;

	@Autowired
	public CmdQueryListener(RoutingGateway gateway) {
		super();
		this.gateway = gateway;
	}

	@ServiceActivator(inputChannel = "execNpmCmdQueryChannel", outputChannel = "msgToJsonConverterChannel")
	public Message<Map<String, String>> handleCmdQuery(@Payload NpmQueryCmd cmd, @Headers Map<String, Object> headers) {
		RestTemplate restTemplate = new RestTemplate();

		ObjectNode result = restTemplate.getForObject("http://registry.npmjs.org/" + cmd.getName(), ObjectNode.class);

		return gateway.sendExtract(buildMessage(new NpmExtractCmd(cmd.getName(), result.toString()), headers));
	}

}
