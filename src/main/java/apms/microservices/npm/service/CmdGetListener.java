package apms.microservices.npm.service;

import static apms.transport.utils.MessageHelper.buildMessage;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

import apms.microservices.npm.domain.NpmGetCmd;
import apms.microservices.npm.domain.NpmModuleRepository;
import apms.microservices.npm.domain.NpmQueryCmd;
import apms.microservices.npm.domain.entities.NpmModule;

@MessageEndpoint
public class CmdGetListener {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private NpmModuleRepository repo;
	private RoutingGateway gateway;

	@Autowired
	public CmdGetListener(NpmModuleRepository repo, RoutingGateway gateway) {
		super();
		this.repo = repo;
		this.gateway = gateway;
	}

	@ServiceActivator(inputChannel = "execNpmCmdGetChannel", outputChannel = "msgToJsonConverterChannel")
	public Message<String> handleCmdGet(@Payload NpmGetCmd cmd, @Headers MessageHeaders headers) {
		NpmModule npmModule = repo.findByName(cmd.getName());

		if (npmModule == null) {
			NpmQueryCmd newCmd = new NpmQueryCmd(cmd.getName());
			Message<NpmQueryCmd> msg = buildMessage(newCmd, headers);
			Message<String> result = gateway.sendQuery(msg);

			npmModule = new NpmModule(cmd.getName(), result.getPayload());
			// this.act(',',{data:seneca.util.clean(npm.data$())})

			try {
				repo.saveAndFlush(npmModule);
				@SuppressWarnings("serial")
				Map<String, String> inserMsg = new HashMap<String, String>() {
					{
						put("role", "search");
						put("cmd", "insert");
					}
				};

				inserMsg.put("data", npmModule.getData());
				gateway.sendSearchInsert(buildMessage(inserMsg, headers));
			} catch (DataIntegrityViolationException e) {

			}

		}

		npmModule = repo.findByName(cmd.getName());

		if (headers.containsKey("replyChannel")) {
			return buildMessage(npmModule.getData(), headers);
		} else {
			return null;
		}

	}
}
