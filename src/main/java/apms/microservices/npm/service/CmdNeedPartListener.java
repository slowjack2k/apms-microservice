package apms.microservices.npm.service;

import static apms.transport.utils.MessageHelper.buildMessage;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

import apms.microservices.npm.domain.InfoCommand;
import apms.microservices.npm.domain.NpmGetCmd;

@MessageEndpoint
public class CmdNeedPartListener {
	private RoutingGateway gateway;

	@Autowired
	public CmdNeedPartListener(RoutingGateway gateway) {
		super();
		this.gateway = gateway;
	}

	@SuppressWarnings("serial")
	@ServiceActivator(inputChannel = "execInfoNeedPartQueryChannel")
	public void handleNeedPart(@Payload InfoCommand infoCmd, @Headers MessageHeaders headers) {
		NpmGetCmd newCmd = new NpmGetCmd(infoCmd.getName());

		Map<String, String> collectCmd = new HashMap<String, String>() {
			{

				{
					put("role", "info");
					put("collect", "part");
					put("part", "npm");
					put("name", infoCmd.getName());
				}
			}
		};

		Message<String> r = gateway.sendGet(buildMessage(newCmd, headers));

		if (r != null && r.getPayload() != null) {
			collectCmd.put("data", r.getPayload());
			gateway.sendCollect(buildMessage(collectCmd, headers));
		}

	}
}
