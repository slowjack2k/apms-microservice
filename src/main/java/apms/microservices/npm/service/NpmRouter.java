package apms.microservices.npm.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Router;

import apms.transport.MsgMatchingRouter;

@MessageEndpoint
public class NpmRouter {
	private MsgMatchingRouter matcher;

	@Autowired
	public NpmRouter(MsgMatchingRouter matcher) {
		super();
		this.matcher = matcher;
	}

	@Router(inputChannel = "npmRoutingJSONOutputChannel")
	public Collection<String> route(String json) {
		return matcher.route(json);
	}
}
