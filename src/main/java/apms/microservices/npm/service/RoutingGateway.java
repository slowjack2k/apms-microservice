package apms.microservices.npm.service;

import java.util.Map;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

import apms.microservices.npm.domain.NpmExtractCmd;
import apms.microservices.npm.domain.NpmGetCmd;
import apms.microservices.npm.domain.NpmQueryCmd;

@MessagingGateway(name = "routingGateway", defaultRequestChannel = "npmRoutingOutputChannel")
public interface RoutingGateway {
	@Gateway(replyTimeout = 10000, replyChannel = "jsonToMapConverterChannel")
	public Message<String> sendQuery(Message<NpmQueryCmd> msg);

	@Gateway(replyTimeout = 10000, replyChannel = "jsonToMapConverterChannel")
	public Message<Map<String, String>> sendExtract(Message<NpmExtractCmd> msg);

	@Gateway(replyTimeout = 10000, replyChannel = "jsonToMapConverterChannel")
	public Message<String> sendGet(Message<NpmGetCmd> msg);

	@Gateway(replyTimeout = 10000, replyChannel = "jsonToMapConverterChannel")
	public void sendCollect(Message<Map<String, String>> msg);

	@Gateway(replyTimeout = 10000, replyChannel = "jsonToMapConverterChannel")
	public void sendSearchInsert(Message<Map<String, String>> msg);
}
