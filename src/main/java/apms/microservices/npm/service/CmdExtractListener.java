package apms.microservices.npm.service;

import static apms.transport.utils.MessageHelper.buildMessage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import apms.microservices.npm.domain.NpmExtractCmd;

@MessageEndpoint
public class CmdExtractListener {
	private final static ObjectMapper mapper = new ObjectMapper();

	@ServiceActivator(inputChannel = "execNpmCmdExtractChannel", outputChannel = "msgToJsonConverterChannel")
	public Message<HashMap<String, String>> handleCmdQuery(@Payload NpmExtractCmd cmd,
			@Headers Map<String, Object> headers) {
		HashMap<String, String> result = new HashMap<String, String>();

		try {
			JsonNode data = mapper.readTree(cmd.getData());
			JsonNode id = data.path("_id");
			JsonNode distTags = data.path("dist-tags");
			JsonNode version = distTags.path("latest");
			JsonNode latest = data.path("versions").path(version.asText());
			JsonNode repository = latest.path("repository");
			JsonNode giturl = repository.path("url");
			JsonNode desc = data.path("description");
			JsonNode readme = data.path("readme");

			result.put("name", id.asText());
			result.put("version", version.asText());
			result.put("giturl", giturl.asText());
			result.put("desc", desc.asText());
			result.put("readme", readme.asText());

		} catch (IOException e) {

		}

		return buildMessage(result, headers);
	}

	@ServiceActivator(inputChannel = "msgToJsonConverterChannel")
	public Message<?> doNothing(Message<?> msg) {

		return msg;
	}
}
