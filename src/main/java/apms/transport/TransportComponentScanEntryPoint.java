package apms.transport;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;

@ComponentScan("apms.transport")
@EnableIntegration
@IntegrationComponentScan("apms.transport")
public class TransportComponentScanEntryPoint {

}
