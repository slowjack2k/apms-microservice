package apms.transport;

import static apms.transport.utils.JSONUtils.JSONtoMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Router;

@MessageEndpoint("router1")
public class MsgMatchingRouter {
	private Collection<List<RoutingMatcher>> groupedMatchers;

	@Autowired
	public MsgMatchingRouter(RoutingMatcher[] matchers) {
		super();

		this.groupedMatchers = java.util.Arrays.asList(matchers).stream()
				.collect(Collectors.groupingBy(RoutingMatcher::group)).values();
	}

	@Router()
	public Collection<String> route(String json) {
		Collection<String> result = new ArrayList<>();
		Map<String, String> map = JSONtoMap(json);

		if (map == null) {
			return java.util.Arrays.asList("errorChannel");
		}

		for (List<RoutingMatcher> matchers : groupedMatchers) {
			Optional<RoutingMatcher> matcher = matchers.stream().filter(m -> m.cntMatches(map) > 0).max((p1, p2) -> {
				System.out.println(p1.cntMatches(map) + " <=> " + p2.cntMatches(map));
				return Integer.compare(p1.cntMatches(map), p2.cntMatches(map));
			});
			if (matcher.isPresent()) {
				result.add(matcher.get().channel());
			}
		}

		if (result.isEmpty()) {
			result = java.util.Arrays.asList("withoutDestination");
		}

		return result;
	}
}