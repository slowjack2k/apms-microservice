package apms.transport.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandlingException;

@MessageEndpoint
public class ErrorListener {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ServiceActivator(inputChannel = "errorChannel")
	public void handleErrors(Message<?> cmdMsg) {
		if (cmdMsg.getPayload().getClass().equals(MessageHandlingException.class)) {
			MessageHandlingException err = (MessageHandlingException) cmdMsg.getPayload();
			logger.error("", err);
		}
		logger.error(cmdMsg.toString());
	}
}
