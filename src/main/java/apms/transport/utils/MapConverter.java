package apms.transport.utils;

import static apms.transport.utils.JSONUtils.JSONtoMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.converter.MessageConverter;

public class MapConverter implements MessageConverter {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private boolean logMsg;

	public MapConverter() {
		this(false);
	}

	public MapConverter(boolean logMsg) {
		super();
		this.logMsg = logMsg;
	}

	@Override
	public Object fromMessage(Message<?> message, Class<?> targetClass) {
		if (logMsg) {
			logger.info("Convert: " + message.getPayload());
		}

		return JSONtoMap((String) message.getPayload());

	}

	@Override
	public Message<?> toMessage(Object payload, MessageHeaders headers) {
		logger.info("Convert: " + payload);
		return null;
	}
}