package apms.transport.utils;

import static apms.transport.utils.JSONUtils.asJSON;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.converter.MessageConverter;

public class StringConverter implements MessageConverter {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public Object fromMessage(Message<?> message, Class<?> targetClass) {
		return asJSON(message.getPayload());
	}

	@Override
	public Message<?> toMessage(Object payload, MessageHeaders headers) {
		return null;
	}
}