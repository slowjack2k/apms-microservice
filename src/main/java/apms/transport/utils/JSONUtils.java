package apms.transport.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONUtils {
	private final static Logger logger = LoggerFactory.getLogger(JSONUtils.class);
	private final static ObjectMapper mapper = new ObjectMapper();

	public static String asJSON(Object msg) {
		try {
			return new ObjectMapper().writeValueAsString(msg);
		} catch (JsonProcessingException e) {
			logger.error("JSON", e);
			throw new IllegalArgumentException(e);
		}
	}

	public static Map<String, String> JSONtoMap(String json) {
		TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
		};

		Map<String, String> map;
		try {
			map = mapper.readValue(json, typeRef);
		} catch (IOException e) {
			logger.error("JSON", e);
			return null;
		}

		return map;
	}

	public static JsonNode JSONtoTree(String json) {

		try {
			return mapper.readTree(Optional.ofNullable(json).orElse("{}"));
		} catch (IOException e) {
			logger.error("JSON", e);
			return mapper.createObjectNode();
		}
	}

	public static Message<String> objMsgToJSONMsg(Message<?> msg) {
		String json;
		try {
			json = mapper.writeValueAsString(msg.getPayload());
		} catch (JsonProcessingException e) {
			logger.error("JSON", e);
			json = "";
		}

		return MessageBuilder.withPayload(json).copyHeadersIfAbsent(msg.getHeaders()).build();
	}
}
