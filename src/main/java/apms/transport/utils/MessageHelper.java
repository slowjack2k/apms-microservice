package apms.transport.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.integration.IntegrationMessageHeaderAccessor;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

public class MessageHelper {
	public static <T> Message<T> buildMessage(T object) {
		final Throwable t = new Throwable();
		t.fillInStackTrace();
		final String methodName = t.getStackTrace()[1].getClassName() + "#" + t.getStackTrace()[1].getMethodName();
		return buildMessage(object, new HashMap<String, Object>(), methodName);
	}

	public static <T> Message<T> buildMessage(T object, Map<String, Object> headers) {
		final Throwable t = new Throwable();
		t.fillInStackTrace();
		final String methodName = t.getStackTrace()[1].getClassName() + "#" + t.getStackTrace()[1].getMethodName();

		return buildMessage(object, headers, methodName);
	}

	public static <T> Message<T> buildMessage(T object, Map<String, Object> headers, String methodName) {
		MessageBuilder<T> builder = MessageBuilder.withPayload(object).copyHeadersIfAbsent(headers);

		if (!headers.containsKey(IntegrationMessageHeaderAccessor.CORRELATION_ID)) {
			builder.setHeader(IntegrationMessageHeaderAccessor.CORRELATION_ID, UUID.randomUUID().toString());
		}

		builder.setHeader("xProducer", methodName);

		return builder.build();

	}
}
