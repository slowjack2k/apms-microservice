package apms.transport.config;

import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.integration.IntegrationMessageHeaderAccessor;
import org.springframework.integration.annotation.Filter;
import org.springframework.integration.channel.AbstractMessageChannel;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.interceptor.WireTap;
import org.springframework.integration.config.EnableMessageHistory;
import org.springframework.integration.config.GlobalChannelInterceptor;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.integration.history.MessageHistory;
import org.springframework.messaging.MessageChannel;

import apms.transport.utils.MapConverter;
import apms.transport.utils.StringConverter;

@Configuration
@EnableMessageHistory
public class TransportDefaultChannels {
	protected static final ExpressionParser EXPRESSION_PARSER = new SpelExpressionParser();

	@Bean
	public MessageChannel errorChannel() {
		return new DirectChannel();
	}

	@Bean
	public MessageChannel jsonToMapConverterChannel() {
		AbstractMessageChannel result = new DirectChannel();
		result.setDatatypes(Map.class);
		result.setMessageConverter(new MapConverter());
		return result;
	}

	@Bean
	public MessageChannel msgToJsonConverterChannel() {
		AbstractMessageChannel result = new DirectChannel();
		result.setDatatypes(String.class);
		result.setMessageConverter(new StringConverter());

		return result;
	}

	@Bean
	public MessageChannel withoutDestination() {
		AbstractMessageChannel result = new DirectChannel();

		return result;
	}

	@Filter(inputChannel = "withoutDestination")
	public boolean rejectAll(Object msg) {
		return false;
	}

	@Bean
	@GlobalChannelInterceptor(patterns = { "!errorChannel", "*" })
	public WireTap tap() {
		Expression expression = EXPRESSION_PARSER.parseExpression("'CORRELATION ID:' + headers?.get('"
				+ IntegrationMessageHeaderAccessor.CORRELATION_ID
				+ "') + '; Header Id:' + headers?.id +'; History: '+ headers?.get('" + MessageHistory.HEADER_NAME
				+ "') +'; Method Id: ' + headers?.get('xProducer') +'; Date:' + headers?.timestamp  + '; ' + ''");
		DirectChannel channel = new DirectChannel();
		channel.setDatatypes(String.class);
		channel.setMessageConverter(new StringConverter());

		LoggingHandler loggingHandler = new LoggingHandler(LoggingHandler.Level.INFO.name());
		loggingHandler.setLogExpression(expression);

		channel.subscribe(loggingHandler);

		return new WireTap(channel);
	}
}
