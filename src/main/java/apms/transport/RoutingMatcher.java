package apms.transport;

import java.util.Map;

public interface RoutingMatcher {

	public String channel();

	public String group();

	public int cntMatches(Map<String, String> msg);
}
