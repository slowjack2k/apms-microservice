package apms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.http.config.EnableIntegrationGraphController;

@SpringBootApplication
@IntegrationComponentScan
@ImportResource()
@EnableIntegrationGraphController(allowedOrigins = "http://localhost:8080")
public class Application {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}

}
