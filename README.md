# APMS

This repository is a proof of concept and reimplements some microservices from  the book the [Tao of Microservices](//www.manning.com/books/the-tao-of-microservices). There by [spring integration](//spring.io/projects/spring-integration) is used to build all microservices within a single process. 

Following conventions are used: 

* all microservices send only messages in JSON format
* every microservice implements classes with the interface `RoutingMatcher`, to signal which message(s) it can handle
* every microservice uses the same router to send messages, no message is delivered directly to another microservice (except through a reply channel) 
* all microservices use only classes from within their package (except the transport package) 

This way it is possible couple all services loosely. Later they can easily be splitted into real microservices. 
Further more it is possible to distribute the current services with the use of distributed channels.

Implemented are at the moment the following services:

* web, a restful API to access infos [original](//github.com/nodezoo/nodezoo-web)
* info, collect data from multiple sources [original](//github.com/nodezoo/nodezoo-info)
* npm, collect data from npm [original](//github.com/nodezoo/nodezoo-npm)
* git, collect data from git [original](//github.com/nodezoo/nodezoo-github)

## info

Uses Redis for Caching

## npm 

Uses a postgreSQL database for permanent data storage.

## git 

Uses a postgreSQL database for permanent data storage.

# License

The MIT License (MIT)
 